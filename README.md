# Montacasa | Frontend Test
##### ANGULAR - MATERIAL - GIT

Bem vindo ao teste para frontend do Montacasa!

Siga as instruções abaixo, leia tudo antes de começar e fique à vontade para fazer perguntas.

## Antes de Iniciar

Pra fazer esse teste, você vai precisar:

  - Ter uma conta configurada no [bitbucket](https://bitbucket.org)

## Configurações iniciais

- Configure as variáveis globais do git na sua máquina com as suas credenciais (complete com o seu nome e email):

        git config --global user.name "John Doe" 
        git config --global user.email johndoe@example.com

- Faça um **fork** do [repositório do teste](https://bitbucket.org/universaldecor/ng-material) para o seu bitbucket (deixe-o público, não privado)
- **Clone** o novo repositório para o computador dentro de um novo diretório, com o seu nome, em `~/teste/`
- Entre no diretório e crie uma nova **branch**, que deve:
  - Ser feita a partir da branch `dev`
  - Ter o nome de `teste/seu-nome`

E depois siga os passos abaixo:

- Instale as dependências com `npm install`
- Inicie a aplicação com `npm run start`
- Navegue em [localhost:4200](http://localhost:4200/)

## Descrição do teste

Você deverá replicar uma página específica, fornecida no momento do teste, dentro dessa aplicação, com a devida adaptação para Material UI.

### É importante:

- A adaptação da página para Material UI utilizando o pacote `@angular/material` previamente instalado
- A correta configuração do Angular conforme for necessário
- A responsividade da tela em questão
- Um estilo de código simples e organizado
- O correto uso de tags HTML5 com ênfase em SEO

### Não é importante:

- O conteúdo da página ser idêntico
- A correta navegação para outras rotas
- Criar um tema para o Material UI
- Outras funcionalidades
- A quantidade ou convenção de commits com o git

### São diferenciais:

- Sugestões de melhorias de UI/UX
- Commits, variáveis e comentários em inglês

## Entrega

A entrega será feita através de um `pull-request`, para isso você deve:

- Subir a branch `teste/seu-nome` no **seu repositório** (o que você criou com o fork)
- Criar o `pull-request`, no bitbucket, da branch `teste/seu-nome` para a `dev` do **repositório original**


- Ao final, lembre-se de remover as suas credenciais das configurações globais do git com os comandos abaixo:

        git config --global --unset user.name 
        git config --global --unset user.email

## Alguns links que podem ajudar

- [Como fazer um fork no bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
- [Documentação do Angular](https://angular.io/docs)
- [Documentação do @angular/material](https://material.angular.io/components)
- [Material Design](https://material.io/)
- [Material UI icons](https://material.io/icons/)
- [Angular no Stackoverflow](https://stackoverflow.com/questions/tagged/angular)
- [Documentação do git](https://www.git-scm.com/doc)
